<?php

print "こんにちわーるど\n";

//for ($i = 0; $i < 10; $i++) {
//    print "start: {$i}\n";
//    sleep(1);
//    print "end: {$i}\n";
//}

for ($i = 0; $i < 10; $i++) {
    $pid = pcntl_fork();
    if ($pid === -1) {
        die('fork できません');
    } else if ($pid) {
        // 親プロセスの場合はなにもしない
    } else {
        // 子プロセスの場合は1秒待って終わる
        print "start: {$i}\n";
        sleep(1);
        print "end: {$i}\n";
        exit();
    }
}
