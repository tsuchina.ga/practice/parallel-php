# phpで並行処理ってどんなかんじ？

## pcntl_forkでプロセスをforkするみたい

とりあえず1秒待機を10回するだけのを試した。


## 環境

* php: 7.2.11
* php-extension: pcntl

実行環境はdockerで用意しています。


## 実行方法

0. コンテナの作成と起動
    
    `docker-compose up -d`

0. 実行

    `php main.php`
